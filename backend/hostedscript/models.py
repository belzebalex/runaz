from django.db import models
import io
import docker
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
import string, random
import os
import shutil

class HostedScript(models.Model):
    uid = models.CharField(max_length=100, primary_key=True)
    requirements = models.TextField(blank=True)
    code = models.TextField(blank=True)
    user_email = models.EmailField(blank=True)
    subdomain = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)
    paid_until = models.DateTimeField(null=True, blank=True)
    service_id = models.TextField(blank=True)

    def _get_service_handle(self):
        cli = docker.from_env()
        return cli.services.get(self.service_id)

    def build(self):
        cli = docker.from_env()
        
        # Random file name
        prefix = ''.join(random.choices(string.ascii_lowercase + string.digits, k=30))
        
        dockerfile_content = (
            "FROM python:3.8-alpine\n"
            "RUN apk add --no-cache --virtual .build-deps libffi-dev openssl-dev gcc musl-dev\n"
            "COPY requirements.txt requirements.txt\n"
            "RUN pip install -r requirements.txt\n"
            "RUN apk del .build-deps gcc musl-dev\n"
            "COPY code.py code.py\n"
            "CMD python3 code.py\n"
        )
        os.mkdir(prefix)
        with open(prefix + "/code.py", "w") as text_file:
            text_file.write(self.code)
        with open(prefix + "/requirements.txt", "w") as text_file:
            text_file.write(self.requirements)
        with open(prefix + "/Dockerfile", "w") as text_file:
            text_file.write(dockerfile_content)
        
        image, logs = cli.images.build(
            path=prefix,
        )
        for line in logs:
            print(line)
       
        image.tag('alextouss/' + self.uid)
       
        for line in cli.images.push('alextouss/' + self.uid, stream=True, decode=True):
            print(line)
        
        shutil.rmtree(prefix)


    def create_service(self):
        if self.service_id != "":
            raise Exception("Service already exists!")
        cli = docker.from_env()
        service = cli.services.create(
            'alextouss/' + self.uid,
            command="python code.py"
        )
        service.scale(0)
        self.service_id = service.id
        self.save(update_fields=['service_id'])
        print("CREATE SERVICE DONE")

    def update_service(self):
        if self.service_id == "":
            return
        cli = docker.from_env()
        service = cli.services.get(self.service_id)
        service.force_update()

    def remove_service(self):
        if self.service_id == "":
            raise Exception("Service is already removed!")
        cli = docker.from_env()
        cli.services.get(self.service_id).remove()
        self.service_id = ""
        self.save()

    def is_running(self):
        if self.service_id == "":
            return False
        handle = self._get_service_handle()
        for task in handle.tasks():
            if task["Status"]["State"] == "running":
                return True
        return False
    
    def start_service(self):
        if self.service_id == "":
            return False
        handle = self._get_service_handle()
        handle.scale(1)

    def stop_service(self):
        if self.service_id == "":
            return False
        handle = self._get_service_handle()
        handle.scale(0)
    
    def get_logs(self):
        if self.service_id == "":
            return "Service Not Launched"

        cli = docker.from_env()
        service = cli.services.get(self.service_id)
        output = ""
        for line in service.logs(stdout=True, stderr=True, tail=10):
            output += line.decode()
        return output


@receiver(post_save, sender=HostedScript)
def build_image_and_create_service(sender, instance, created, **kwargs):
    if created:
        instance.build()
        instance.create_service()


@receiver(pre_save, sender=HostedScript)
def build_image_and_update_service(sender, instance, **kwargs):
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass  # Object is new
    else:
        if not (obj.code == instance.code and obj.requirements == instance.requirements):  # Field has changed
            instance.build()
            instance.update_service()
            print("UPDATE SERVICE ENDED!")
        

