from rest_framework import viewsets
from .serializers import HostedScriptSerializer
from .models import HostedScript
from django.http import HttpResponse, HttpResponseBadRequest


class HostedScriptViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = HostedScript.objects.all()
    serializer_class = HostedScriptSerializer


def start(request, uid):
    script = HostedScript.objects.get(uid=uid)
    script.start_service()
    return HttpResponse()


def stop(request, uid):
    script = HostedScript.objects.get(uid=uid)
    script.stop_service()
    return HttpResponse()


def get_status(request, uid):
    script = HostedScript.objects.get(uid=uid)
    return HttpResponse("RUNNING" if script.is_running() else "STOPPED")


def get_logs(request, uid):
    script = HostedScript.objects.get(uid=uid)
    return HttpResponse(script.get_logs())
