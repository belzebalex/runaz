from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers, serializers, viewsets

from . import views
router = routers.DefaultRouter()
router.register(r'scripts', views.HostedScriptViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    path('start/<uid>/', views.start),
    path('stop/<uid>/', views.stop),
    path('logs/<uid>/', views.get_logs),
    path('status/<uid>/', views.get_status),
]
