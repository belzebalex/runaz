from .models import HostedScript
from rest_framework import serializers


# TODO: Implement verifications on
# - Code Content (eg. no ENDOFILE in it)
# - subdomain (only chars)
class HostedScriptSerializer(serializers.ModelSerializer):
    class Meta:
        model = HostedScript
        fields = ['uid', 'requirements', 'code', 'user_email', 'subdomain']

