import React, { useEffect, useState } from "react"
import { useHistory, useParams } from "react-router-dom";
import { SCRIPT_ROUTE_URL } from "../api/constants"

import axios from "axios"
import codeExamples from "../utils/premade-strategies.js"
import AceEditor from "react-ace";

import ScriptStatus from "./ScriptStatus"

import "ace-builds/src-noconflict/mode-python";
import "ace-builds/src-noconflict/theme-monokai";



export default function EditScript() {
    let { uid } = useParams(); 
    
    const [premadeCodeName, setPremadeCodeName] = useState("")
    const [isLoading, setLoading] = useState(true)
    const [fetchedState, setFetchedState] = useState(null)
    const [formState, setFormState] = useState({
        code: "",
        requirements: "",
        subdomain: "",
        uid: uid
    })
    
    const getNewScript = () => {
        axios.get(SCRIPT_ROUTE_URL + uid)
        .then(res => {
            setFormState(res.data)
            setFetchedState(res.data)
            setLoading(false)
        })
    }

    const putNewScript = (obj) => {
        axios.put(SCRIPT_ROUTE_URL + obj.uid + "/", obj)
            .then(res => {
                console.log(res.data)
                setFetchedState(res.data)
            })
    }

    const updateFormField = (field, value) => {
        if(field === "subdomain") {
            if (/[^a-z]/i.test(value) || value.length > 18) {
                return
            }
        }
        setFormState({...formState, [field]: value})
        if ( field === "requirements" || field === "code") {
            setPremadeCodeName("")
        }
        console.log(formState)
    }
    
    useEffect(() => {
        getNewScript()
    }, [])

    if(isLoading) {
        return <progress className="progress is-small is-primary" max="100">15%</progress>
    }

    const premadeCodeList = codeExamples.map(example => 
        <a 
            className={"dropdown-item " + (example[0] === premadeCodeName ? "is-active" : "")}
            onClick={() => {
                setPremadeCodeName(example[0])
                setFormState({...formState, code: example[1], requirements: example[2]})
            }}
        >
            {example[0]}<br/>        
        </a>
    )

    return (
        <>
        <div  className="container has-text-centered">
            <div className="section">
                <div className="columns is-vcentered">
                    <div className="column is-12">
                        <div class="dropdown is-hoverable">
                            <div class="dropdown-trigger">
                                <button class="button" aria-haspopup="true" aria-controls="dropdown-menu4">
                                    <span>Example Snippets</span>
                                    <span class="icon is-small">
                                        <i class="fas fa-angle-down" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                            <div class="dropdown-menu" id="dropdown-menu4" role="menu">
                                <div className="dropdown-content">
                                    {premadeCodeList}
                                </div>
                            </div>
                        </div>
                        <div className="columns" style={{marginTop: 20}}>
                            <div className="column is-6">
                                <p className="has-text-left">Script</p>
                                <AceEditor
                                    mode="python"
                                    theme="monokai"
                                    onChange={e => updateFormField("code", e)}
                                    value={formState.code}
                                    name="UNIQUE_ID_OF_DIV"
                                    editorProps={{ $blockScrolling: true }}
                                    style={{width: "100%", borderRadius: 5}}
                                    showPrintMargin={false}
                                    fontSize={16}
                                    rows={20}
                                />
                                <br/>
                            </div>
                            <div className="column is-6">
                                <p className="is-pulled-left">Requirements</p>
                                <AceEditor
                                    mode="text"
                                    theme="monokai"
                                    onChange={e => updateFormField("requirements", e)}
                                    value={formState.requirements}
                                    name="UNIQUE_ID_OF_DIV_2"
                                    editorProps={{ $blockScrolling: true }}
                                    style={{
                                        width: "100%", 
                                        height:"100px",  
                                        borderRadius: 5, 
                                        borderColor: "#000", 
                                        borderStyle: "solid",
                                        borderWidth: 1,
                                    }}
                                    showPrintMargin={false}
                                    fontSize={16}
                                    showGutter={false}
                                />
                                
                                <ScriptStatus uid={uid}  />
                                
                                <div style={{marginLeft: 20, marginTop: 30}}>
                                    <div className="field has-addons is-centered">
                                        <p className="control" >
                                            <input value={formState.subdomain} onChange={e => updateFormField("subdomain", e.target.value)} className="input" type="text" placeholder="Your Subdomain" />
                                        </p>
                                        <p className="control">
                                            <a className="button is-static">
                                                .runaz.io
                                            </a>
                                        </p>
                                    </div>
                                    {JSON.stringify(formState) === JSON.stringify(fetchedState) ? (
                                        <button className="button is-link is-pulled-left" disabled onClick={() => putNewScript(formState)}>
                                            <span>Save</span>
                                        </button>
                                    ) : (
                                        <>
                                        <button className="button is-link is-pulled-left"  onClick={() => putNewScript(formState)}>
                                            <span>Save</span>
                                        </button>
                                        <button className="button is-pulled-left" style={{marginLeft: 10}} onClick={() => getNewScript(formState)}>
                                            <span>Cancel</span>
                                        </button>
                                        </>
                                    )}
                                </div><br/>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
        </>
)
}

