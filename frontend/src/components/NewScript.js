import React, { useState } from "react"
import { useHistory } from "react-router-dom";
import { SCRIPT_ROUTE_URL } from "../api/constants"
import axios from "axios"
import codeExamples from "../utils/premade-strategies.js"
import makeid from "../utils/makeid"
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-python";
import "ace-builds/src-noconflict/theme-monokai";



export default function NewScript() {
    const [premadeCodeName, setPremadeCodeName] = useState(codeExamples[0][0])
    let history = useHistory();

    const [formState, setFormState] = useState({
        code: codeExamples[0][1],
        requirements: codeExamples[0][2],
        subdomain: "",
        uid: makeid(20)
    })


    const postNewScript = (obj) => {
        axios.post(SCRIPT_ROUTE_URL, obj) 
            .then(() => {
                history.push("/scripts/" + obj.uid);
        })
    }

    
    const premadeCodeList = codeExamples.map(example => 
        <a 
            className={"dropdown-item " + (example[0] === premadeCodeName ? "is-active" : "")}
            onClick={() => {
                setPremadeCodeName(example[0])
                setFormState({...formState, code: example[1], requirements: example[2]})
            }}
        >
            {example[0]}<br/>        
        </a>
    )

    return (
        <>
        <div  className="container has-text-centered">
            <div className="section">
                <div className="columns is-vcentered">
                    <div className="column is-12">
                        <div style={{margin: 30, marginBottom: 60}}>
                            <h1 className="title is-2">
                                One-click python script hosting
                            </h1>
                            <h2 className="subtitle is-4">
                                Enter your code, Az hosts it 
                                in seconds. <br/>
                                No setup, no CI, no CD. 
                                You don't have anything to learn.
                            </h2>
                        </div>
                        <div className="columns is-vcentered">
                            <div className="column is-6">
                                <div class="dropdown is-hoverable">
                                    <div class="dropdown-trigger">
                                        <button class="button" aria-haspopup="true" aria-controls="dropdown-menu4">
                                            <span>{premadeCodeName}</span>
                                            <span class="icon is-small">
                                                <i class="fas fa-angle-down" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="dropdown-menu" id="dropdown-menu4" role="menu">
                                        <div class="dropdown-content">
                                            {premadeCodeList}
                                        </div>
                                    </div>
                                </div>
                                <p className="has-text-left">Script</p>
                                <AceEditor
                                    mode="python"
                                    theme="monokai"
                                    onChange={e => setFormState({...formState, code: e})}
                                    value={formState.code}
                                    name="UNIQUE_ID_OF_DIV"
                                    editorProps={{ $blockScrolling: true }}
                                    style={{width: "100%", borderRadius: 5}}
                                    showPrintMargin={false}
                                    fontSize={16}
                                    rows={20}
                                />
                                <br/>
                                <p className="is-pulled-left">Requirements</p>
                                <AceEditor
                                    mode="text"
                                    theme="monokai"
                                    onChange={e => setFormState({...formState, requirements: e})}
                                    value={formState.requirements}
                                    name="UNIQUE_ID_OF_DIV_2"
                                    editorProps={{ $blockScrolling: true }}
                                    style={{
                                        width: "100%", 
                                        height:"100px",  
                                        borderRadius: 5, 
                                        borderColor: "#000", 
                                        borderStyle: "solid",
                                        borderWidth: 1,
                                    }}
                                    showPrintMargin={false}
                                    fontSize={16}
                                    showGutter={false}
                                />
                            </div>
                            <div className="column is-6">
                                <div style={{marginLeft: 20}}>
                                    <div className="content has-text-left">
                                        <span className="icon is-medium has-text-link">
                                            <i className="fas fa-lock fa-lg "></i>
                                        </span>
                                        <span className="is-size-4">Free Subdomain with SSL</span>
                                        <br/>
                                        <em>&nbsp;&nbsp;Ex: mystats.runaz.io</em>
                                    </div>
                                    <div className="content has-text-left">
                                        <span className="icon is-medium has-text-link">
                                            <i className="fas fa-hdd fa-lg "></i>
                                        </span>
                                        <span className="is-size-4">1 GB storage</span>
                                        <br/>
                                        <em>&nbsp;&nbsp;Well enough for all your uses</em>
                                    </div>
                                    <div className="content has-text-left">
                                        <span className="icon is-medium has-text-link">
                                            <i className="fas fa-dollar-sign fa-lg "></i>
                                        </span>
                                        <span className="is-size-4">Clear Pricing</span>
                                        <br/>
                                        <em>&nbsp;&nbsp;$1/month</em>
                                    </div>
                                    <br/>
                                    <div className="field has-addons is-centered">
                                        <p className="control" value={formState.subdomain}>
                                            <input onChange={e => setFormState({...formState, subdomain: e.target.value})} className="input" type="text" placeholder="Your Subdomain" />
                                        </p>
                                        <p className="control">
                                            <a className="button is-static">
                                                .runaz.io
                                            </a>
                                        </p>
                                    </div>
                                    <button className="button is-link is-pulled-left" onClick={() => postNewScript(formState)}>
                                        <span className="icon is-small">
                                            <i className="fas fa-plus"></i>
                                        </span>
                                        <span>Host Script</span>
                                    </button>
                                </div><br/>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
        </>
)
}

