import React, { useState, useEffect } from "react"
import axios from "axios"

import { 
    SCRIPT_LOGS_URL, 
    SCRIPT_STATUS_URL, 
    SCRIPT_START_URL, 
    SCRIPT_STOP_URL 
} from "../api/constants"

export default function ScriptStatus({uid}) {

    const [status, setStatus] = useState({
        running: false,
        logs: "",
    })

    const refreshData = () => {
        axios.get(SCRIPT_STATUS_URL + uid)
            .then(res1 => {
                axios.get(SCRIPT_LOGS_URL + uid)
                    .then(res2 => {
                        setStatus({
                            running: res1.data === "RUNNING",
                            logs: res2.data
                        })    
                    })
            })
    }

    const stop = () => {
        axios.get(SCRIPT_STOP_URL + uid + "/")
    }

    const start = () => {
        axios.get(SCRIPT_START_URL + uid + "/")
    }

    useEffect(() => {
        let interval = setInterval(()=>{
            refreshData()
        }, 1000)
        return () => clearInterval(interval);
    }, [])

    return (
        <div className="box" style={{marginTop: 30, textAlign: "left"}}>
            <button 
                onClick={() => stop()}
                className={status.running ? "button is-danger is-small" : "is-hidden"}
            >
                <span className="icon is-small">
                    <i className="fas  fa-stop"></i>
                </span>
            <span>Pause</span>
            </button>
            <button 
                className={status.running ? "is-hidden": "button is-link is-small"}
                onClick={() => start()}
            >
                <span className="icon is-small">
                    <i className="fas fa-play"></i>
                </span>
                <span>Start</span>
            </button>
            <p>Status: {status.running ? "Running" : "Stopped"}</p>
            <p>Logs:</p>
            <code>
            {status.logs.split('\n').map(function(item, key) {
                return (
                    <span key={key}>
                        {item}
                        <br/>
                    </span>
                )
            })}
        </code>
        </div>
    )
}
