import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";
import NewScript from "./NewScript"
import EditScript from "./EditScript"

export default function App() {
    return (
        <>
        <Router>
            <div className="container">
                <div className="navbar">
                    <div className="navbar-brand">
                        <div className="navbar-item">
                            <h1 className="title">
                                Az
                            </h1>
                        </div>
                        <span class="navbar-burger burger" data-target="navbarMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div className="navbar-menu" style={{marginTop: 7}}>
                        <div className="navbar-end">
                                <div className="tabs is-right">
                                    <li>
                                        <NavLink activeClassName='is-active' to="/">
                                            New Script
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink activeClassName='is-active' to="/features">
                                            Features
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink activeClassName='is-active' to="/faq">
                                            FAQ
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink activeClassName='is-active' to="/about">
                                            About
                                        </NavLink>
                                    </li>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <Switch>
                    <Route path="/scripts/:uid">
                        <EditScript />
                    </Route>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/faq">
                        <About />
                    </Route>
                    <Route path="/features">
                        <About />
                    </Route>
                    <Route path="/">
                        <NewScript />
                    </Route>
                </Switch>
            </div>
        </Router>
        </>
    );
}

function Home() {
    return <h2>Home</h2>;
}

function About() {
    return (
        <div className="is-container">
            <h1 className="is-heading">About</h1>hey;
        </div>
    )
}

function Topics() {
    let match = useRouteMatch();

    return (
        <div>
            {/* The Topics page has its own <Switch> with more routes
          that build on the /topics URL path. You can think of the
          2nd <Route> here as an "index" page for all topics, or
          the page that is shown when no topic is selected */}
          <Switch>
              <Route path={`${match.path}/:topicId`}>
                  <Topic />
              </Route>
              <Route path={match.path}>
                  <h3>Please select a topic.</h3>
              </Route>
          </Switch>
      </div>
    );
}

function Topic() {
    let { topicId } = useParams();
    return <h3>Requested topic ID: {topicId}</h3>;
}

